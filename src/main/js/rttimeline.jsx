const React = require( 'react' );
const ReactDOM = require( 'react-dom' );
const client = require( './client.jsx' );
process.hrtime = require( 'browser-process-hrtime' );
import getSpeed from 'fast-speed-test';
import C3Chart from 'react-c3js';
import 'c3/c3.css';
import moment from "moment";

export default class Timeline extends React.Component {
  constructor( props ) {
    super( props );

    this.state = {
      data: {
        x: 'x',
        xFormat: '%H:%M:%S',
        columns: [
        ]
      },
      axis: {
        x: {
          type: 'timeseries',
          tick: {
            format: '%H:%M:%S'
          }
        }
      },
      rsrpData: {
        columns: [
          ['rsrp', -130]
        ],
        type: 'gauge'
      },
      rsrqData: {
        columns: [
          ['rsrq', -15]
        ],
        type: 'gauge'
      },
      rssiData: {
        columns: [
          ['rssi', -90.0]
        ],
        type: 'gauge'
      },
      snrData: {
        columns: [
          ['snr', 0.0]
        ],
        type: 'gauge'
      },
      speedData: {
        columns: [
          ['Speed', 60.0]
        ],
        type: 'gauge'
      },
      speedStart: 0,
      speedEnd: 0,
      speedTestRunning: false,
    }
  }

  tick() {
    client( {
      method: 'GET',
      path: '/api/latestData'
    } ).then( response => {
      var entity = response.entity;
      var data = this.state.data;
      this.getColumn( data, "x" ).push( moment().format( "HH:mm:ss" ) );

      var rsrp = entity.rsrp;
      var rsrpData = this.state.rsrpData;
      this.getColumn( data, "rsrp" ).push( rsrp );
      rsrpData.columns[0].pop();
      rsrpData.columns[0].push( rsrp );

      var rsrq = entity.rsrq;
      var rsrqData = this.state.rsrqData;
      this.getColumn( data, "rsrq" ).push( rsrq );
      rsrqData.columns[0].pop();
      rsrqData.columns[0].push( rsrq );

      var rssi = entity.rssi;
      var rssiData = this.state.rssiData;
      this.getColumn( data, "rssi" ).push( rssi );
      rssiData.columns[0].pop();
      rssiData.columns[0].push( rssi );

      var snr = entity.snr;
      var snrData = this.state.snrData;
      this.getColumn( data, "snr" ).push( snr );
      snrData.columns[0].pop();
      snrData.columns[0].push( snr );

      this.setState( { data: data, rsrpData: rsrpData, rsrqData: rsrqData, rssiData: rssiData, snrData: snrData } );
    }, errorHandling => {
      console.log( "Error" );
    } );
  }

  getColumn( data, column ) {
    var foundColumn = null;
    for ( var i = 0; i < data.columns.length; i++ ) {
      if ( data.columns[i][0] === column ) {
        foundColumn = data.columns[i];
        break;
      }
    }

    if ( foundColumn === null ) {
      var length = data.columns.push( [column] );
      foundColumn = data.columns[length - 1];
    } else if ( foundColumn.length > 120 ) {
      foundColumn.splice( 1, 1 );
    }

    return foundColumn;
  }

  componentDidMount() {
    this.intervalID = setInterval(() => this.tick(), 1000 );
    // this.speedTestIntervalID = setInterval(() => this.speedTest(), 5000 );
    // setInterval(() => this.forceUpdate(), 1000 );
    this.speedTest();
  }

  speedTest() {
    if ( false === true ) {
      console.log( "Starting speedtest" );
      this.setState( { speedTestRunning: true, speedStart: moment().format( "HH:mm:ss" ) } );

      // Speedtest
      var speedTest = require( 'speedtest-net' );
      var test = speedTest( { maxTime: 30000, serverId: 5055 } );

      test.on( 'downloadprogress', progress => {
        console.log( 'Download progress:', progress );
        if ( progress >= 100 ) {
          this.setState( { speedTestRunning: false, speedEnd: moment().format( "HH:mm:ss" ) } );
        }
      } );

      test.on( 'downloadspeedprogress', speed => {
        console.log( 'Download speed (in progress):', ( speed * 125 ).toFixed( 2 ), 'Kbps' );
        console.log( 'Download speed (in progress):', speed, 'Mbps' );
        var speedData = this.state.speedData;
        speedData.columns[0].pop();
        speedData.columns[0].push( speed );
        this.setState( { speedData: speedData } );
      } );

      test.on( 'result', url => {
        if ( !url ) {
          console.log( 'Could not successfully post test results.' );
        } else {
          console.log( 'Test result url:', url );
        }
      } );

      test.on( 'data', data => {
        console.dir( data );
      } );

      test.on( 'error', err => {
        console.error( err );
      } );

      // Fast.com
      //      getSpeed().then( speed => {
      //        console.log( speed + ' bytes per second' );
      //        var mbps = speed / ( 2 ** 17 );
      //        console.log( mbps + ' Mbps' );
      //        var speedData = this.state.speedData;
      //        speedData.columns[0].pop();
      //        speedData.columns[0].push( mbps );
      //        this.setState( { speedTestRunning: false, speedData: speedData, speedEnd: moment().format( "HH:mm:ss" ) } );
      //      } );

    } else {
      console.log( "Test on going... wait for it..." );
    }
  }

  render() {
    var pattern = ['#FF0000', '#FF8800', '#FFFA00', '#60B044'];
    var labelFormatFunction = function( value, ratio ) {
      return value;
    };

    return (
      <div>
        <div id="table" style={{ display: "table", width: "100vw" }}>
          <div id="table-row" style={{ display: "table-row" }}>
            <div id="table-cell" style={{ display: "table-cell", width: "50vw" }}>
              <C3Chart data={this.state.rsrpData} color={{
                pattern: pattern,
                threshold: {
                  unit: 'value',
                  max: -70,
                  values: [-110, -100, -80, -70]
                }
              }}
                gauge={{
                  label: {
                    format: labelFormatFunction
                  },
                  min: -130,
                  max: -70,
                  units: ' dBm'
                }} />
            </div>
            <div id="table-cell" style={{ display: "table-cell", width: "50vw" }}>
              <C3Chart data={this.state.rsrqData} color={{
                pattern: pattern,
                threshold: {
                  unit: 'value',
                  max: -3,
                  values: [-11, -8, -5, -3]
                }
              }}
                gauge={{
                  label: {
                    format: labelFormatFunction
                  },
                  min: -15,
                  max: -3,
                  units: ' dB'
                }} />
            </div>
          </div>
          <div id="table-row" style={{ display: "table-row" }}>
            <div id="table-cell" style={{ display: "table-cell" }}>
              <C3Chart data={this.state.rssiData} color={{
                pattern: pattern,
                threshold: {
                  unit: 'value',
                  max: -60,
                  values: [-85, -75, -65, -60]
                }
              }}
                gauge={{
                  label: {
                    format: labelFormatFunction
                  },
                  min: -90,
                  max: -65,
                  units: ''
                }} />
            </div>
            <div id="table-cell" style={{ display: "table-cell" }}>
              <C3Chart data={this.state.snrData} color={{
                pattern: pattern,
                threshold: {
                  unit: 'value',
                  max: 30,
                  values: [10, 15, 20, 30]
                }
              }}
                gauge={{
                  label: {
                    format: labelFormatFunction
                  },
                  min: 0,
                  max: 30,
                  units: ' dB'
                }} />
            </div>
          </div>
        </div>

        <C3Chart data={this.state.data} axis={this.state.axis} />
        <C3Chart data={this.state.speedData} color={{
          pattern: pattern,
          threshold: {
            unit: 'value',
            max: 150,
            values: [10, 30, 60, 100]
          }
        }}
          gauge={{
            label: {
              format: labelFormatFunction
            },
            min: 0,
            max: 150,
            units: ' Mbps'
          }} />
        Started: {this.state.speedStart}, Last end: {this.state.speedEnd}
      </div>
    );
  }
}
