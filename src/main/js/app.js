const React = require('react');
const ReactDOM = require('react-dom');
import Timeline from './rttimeline.jsx';

class ZyxelStats extends React.Component {
	
  render() {
	  return (
			  <div>
			  <Timeline />
			  </div>
	        );
  }
}

ReactDOM.render(<ZyxelStats />, document.getElementById('react'));
