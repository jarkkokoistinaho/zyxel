package zyxel;

import java.io.IOException;
import java.io.StringReader;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.time.Instant;
import java.util.Iterator;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import zyxel.domain.ConnectionInformation;
import zyxel.domain.Data;
import zyxel.domain.Error;
import zyxel.repository.DataRepository;

@Controller
public class FetchController {

  private static final int TIMEOUT_MS = 1000;

  private String zyxelUrl;

  @Autowired
  private DataRepository repository;

  private Data latestData;

  public FetchController(@Value("${zyxel.url}") String zyxelUrl) {
    this.zyxelUrl = zyxelUrl;
  }

  @Scheduled(initialDelay = 1000, fixedDelay = 1000)
  public void pollData()
      throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException {
    String url = zyxelUrl + "/cgi-bin/gui.cgi?_=0.5773941713384309";
    System.out.println("Plaa: " + zyxelUrl);
    RestTemplate createNewRestTemplate = createNewRestTemplate();
    HttpHeaders headers = new HttpHeaders();
    String json2 = "{\"action\":\"get_wwan_network_internet_status\",\"args\":{}}";
    HttpEntity<String> requestEntity = new HttpEntity<>(json2, headers);
    String body =
        createNewRestTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class).getBody();

    ObjectMapper mapper = new ObjectMapper();
    JsonNode jsonNode =
        mapper.readTree(new StringReader(body)).get("get_wwan_network_internet_status");
    ((ObjectNode) jsonNode).put("timestamp", Instant.now().toString());

    saveData(mapper, jsonNode);
  }

  @Transactional
  public void saveData(ObjectMapper mapper, JsonNode jsonNode)
      throws IOException, JsonParseException, JsonMappingException {
    Data data = mapper.readValue(jsonNode.toString(), Data.class);
    ConnectionInformation connectionInformation =
        mapper.readValue(jsonNode.toString(), ConnectionInformation.class);
    Error error = mapper.readValue(jsonNode.toString(), Error.class);

    boolean connectionInformationChanged = false;
    boolean errorChanged = false;

    Iterator<Data> iterator = repository.findAll().iterator();
    while (iterator.hasNext()) {
      if (true)
        break;
      Data next = iterator.next();
      if (!connectionInformationChanged
          && connectionInformation.equals(next.getConnectionInformation())) {
        connectionInformation = next.getConnectionInformation();
        connectionInformationChanged = true;
      }

      if (!errorChanged && error.equals(next.getError())) {
        error = next.getError();
        errorChanged = true;
      }

      if (connectionInformationChanged && errorChanged) {
        break;
      }
    }

    data.setConnectionInformation(connectionInformation);
    data.setError(error);

    System.out.println("-------------------------------------");
    System.out.println(connectionInformation);
    System.out.println(data);
    System.out.println(error);
    // repository.save(data);
    latestData = data;
  }

  private RestTemplate createNewRestTemplate()
      throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
    HttpComponentsClientHttpRequestFactory httpComponentsClientHttpRequestFactory =
        new HttpComponentsClientHttpRequestFactory();
    httpComponentsClientHttpRequestFactory.setConnectionRequestTimeout(TIMEOUT_MS);
    httpComponentsClientHttpRequestFactory.setConnectTimeout(TIMEOUT_MS);
    httpComponentsClientHttpRequestFactory.setReadTimeout(TIMEOUT_MS);

    TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

    SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
        .loadTrustMaterial(null, acceptingTrustStrategy).build();

    SSLConnectionSocketFactory csf =
        new SSLConnectionSocketFactory(sslContext, new NoopHostnameVerifier());
    CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
    httpComponentsClientHttpRequestFactory.setHttpClient(httpClient);
    return new RestTemplate(httpComponentsClientHttpRequestFactory);
  }

  public Data getLatestData() {
    return latestData;
  }

}
