package zyxel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
// @EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class,
// DataSourceTransactionManagerAutoConfiguration.class,
// HibernateJpaAutoConfiguration.class })
@EnableJpaRepositories
@EnableScheduling
public class ZyxelApplication {

  public static void main(String[] args) throws Exception {
    SpringApplication.run(new Object[] { ZyxelApplication.class }, args);
  }
}
