package zyxel.repository;

import org.springframework.data.repository.CrudRepository;

import zyxel.domain.Data;

public interface DataRepository extends CrudRepository<Data, Long> {

}
