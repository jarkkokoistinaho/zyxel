package zyxel.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class ConnectionInformation implements LTEInformationHandler, Serializable {
  private static final long serialVersionUID = 6789346186213583096L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private Integer state;
  private String ip;
  private String dns;
  private String dns2;
  private Integer ipv6_state;
  private String ipv6_ip;
  private String ipv6_dns;
  private String ipv6_dns2;
  private String operator;
  private Integer mcc;
  private Integer mnc;
  private String type;
  private String band;
  private Integer chnel;
  private Integer tac;
  private Integer cid;
  private Integer pci;

  public Integer getState() {
    return state;
  }

  public String getIp() {
    return ip;
  }

  public String getDns() {
    return dns;
  }

  public String getDns2() {
    return dns2;
  }

  public Integer getIpv6_state() {
    return ipv6_state;
  }

  public String getIpv6_ip() {
    return ipv6_ip;
  }

  public String getIpv6_dns() {
    return ipv6_dns;
  }

  public String getIpv6_dns2() {
    return ipv6_dns2;
  }

  public String getOperator() {
    return operator;
  }

  public Integer getMcc() {
    return mcc;
  }

  public Integer getMnc() {
    return mnc;
  }

  public String getType() {
    return type;
  }

  public String getBand() {
    return band;
  }

  public Integer getChnel() {
    return chnel;
  }

  public Integer getTac() {
    return tac;
  }

  public Integer getCid() {
    return cid;
  }

  public Integer getPci() {
    return pci;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((band == null) ? 0 : band.hashCode());
    result = prime * result + ((chnel == null) ? 0 : chnel.hashCode());
    result = prime * result + ((cid == null) ? 0 : cid.hashCode());
    result = prime * result + ((dns == null) ? 0 : dns.hashCode());
    result = prime * result + ((dns2 == null) ? 0 : dns2.hashCode());
    result = prime * result + ((ip == null) ? 0 : ip.hashCode());
    result = prime * result + ((ipv6_dns == null) ? 0 : ipv6_dns.hashCode());
    result = prime * result + ((ipv6_dns2 == null) ? 0 : ipv6_dns2.hashCode());
    result = prime * result + ((ipv6_ip == null) ? 0 : ipv6_ip.hashCode());
    result = prime * result + ((ipv6_state == null) ? 0 : ipv6_state.hashCode());
    result = prime * result + ((mcc == null) ? 0 : mcc.hashCode());
    result = prime * result + ((mnc == null) ? 0 : mnc.hashCode());
    result = prime * result + ((operator == null) ? 0 : operator.hashCode());
    result = prime * result + ((pci == null) ? 0 : pci.hashCode());
    result = prime * result + ((state == null) ? 0 : state.hashCode());
    result = prime * result + ((tac == null) ? 0 : tac.hashCode());
    result = prime * result + ((type == null) ? 0 : type.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ConnectionInformation other = (ConnectionInformation) obj;
    if (band == null) {
      if (other.band != null)
        return false;
    } else if (!band.equals(other.band))
      return false;
    if (chnel == null) {
      if (other.chnel != null)
        return false;
    } else if (!chnel.equals(other.chnel))
      return false;
    if (cid == null) {
      if (other.cid != null)
        return false;
    } else if (!cid.equals(other.cid))
      return false;
    if (dns == null) {
      if (other.dns != null)
        return false;
    } else if (!dns.equals(other.dns))
      return false;
    if (dns2 == null) {
      if (other.dns2 != null)
        return false;
    } else if (!dns2.equals(other.dns2))
      return false;
    if (ip == null) {
      if (other.ip != null)
        return false;
    } else if (!ip.equals(other.ip))
      return false;
    if (ipv6_dns == null) {
      if (other.ipv6_dns != null)
        return false;
    } else if (!ipv6_dns.equals(other.ipv6_dns))
      return false;
    if (ipv6_dns2 == null) {
      if (other.ipv6_dns2 != null)
        return false;
    } else if (!ipv6_dns2.equals(other.ipv6_dns2))
      return false;
    if (ipv6_ip == null) {
      if (other.ipv6_ip != null)
        return false;
    } else if (!ipv6_ip.equals(other.ipv6_ip))
      return false;
    if (ipv6_state == null) {
      if (other.ipv6_state != null)
        return false;
    } else if (!ipv6_state.equals(other.ipv6_state))
      return false;
    if (mcc == null) {
      if (other.mcc != null)
        return false;
    } else if (!mcc.equals(other.mcc))
      return false;
    if (mnc == null) {
      if (other.mnc != null)
        return false;
    } else if (!mnc.equals(other.mnc))
      return false;
    if (operator == null) {
      if (other.operator != null)
        return false;
    } else if (!operator.equals(other.operator))
      return false;
    if (pci == null) {
      if (other.pci != null)
        return false;
    } else if (!pci.equals(other.pci))
      return false;
    if (state == null) {
      if (other.state != null)
        return false;
    } else if (!state.equals(other.state))
      return false;
    if (tac == null) {
      if (other.tac != null)
        return false;
    } else if (!tac.equals(other.tac))
      return false;
    if (type == null) {
      if (other.type != null)
        return false;
    } else if (!type.equals(other.type))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return operator + "/" + cid + "/" + band;
  }

}
