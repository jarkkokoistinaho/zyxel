package zyxel.domain;

import java.lang.reflect.Field;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public interface LTEInformationHandler {

  @JsonProperty("lte")
  public default void unpackNested(Map<String, Object> lte) throws IllegalArgumentException, IllegalAccessException {
    for (Field field : this.getClass().getDeclaredFields()) {
      Object object = lte.get(field.getName());
      if (object != null) {
        field.setAccessible(true);
        field.set(this, object);
        field.setAccessible(false);
      }
    }
  }

}
