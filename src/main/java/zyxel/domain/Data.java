package zyxel.domain;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import zyxel.json.InstantDeserializer;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class Data implements LTEInformationHandler, Serializable {
  private static final long serialVersionUID = 6015816821686951574L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private @OneToOne(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER) ConnectionInformation connectionInformation;
  private @OneToOne(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER, orphanRemoval = true) Error error;
  private String connection_time;
  private String ipv6_connection_time;
  private Integer rsrp;
  private Integer rsrq;
  private Integer snr;
  private Integer cqi;
  private Integer rssi;
  @JsonDeserialize(using = InstantDeserializer.class)
  private Instant timestamp;

  public String getConnection_time() {
    return connection_time;
  }

  public String getIpv6_connection_time() {
    return ipv6_connection_time;
  }

  public Integer getRsrp() {
    return rsrp;
  }

  public Integer getRsrq() {
    return rsrq;
  }

  public Integer getSnr() {
    return snr;
  }

  public Integer getCqi() {
    return cqi;
  }

  public Integer getRssi() {
    return rssi;
  }

  public Instant getTimestamp() {
    return timestamp;
  }

  public ConnectionInformation getConnectionInformation() {
    return connectionInformation;
  }

  public void setConnectionInformation(ConnectionInformation connectionInformation) {
    this.connectionInformation = connectionInformation;
  }

  public Error getError() {
    return error;
  }

  public void setError(Error error) {
    this.error = error;
  }

  @Override
  public String toString() {
    return "RSSI:" + rssi + "/SNR:" + snr + "/RSRQ:" + rsrq + "/RSRP:" + rsrp;
  }
}
