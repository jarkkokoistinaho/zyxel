package zyxel.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class Error implements Serializable {
  private static final long serialVersionUID = -95714097748376673L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private Integer errno;
  private String errmsg;

  public Integer getErrno() {
    return errno;
  }

  public String getErrmsg() {
    return errmsg;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((errmsg == null) ? 0 : errmsg.hashCode());
    result = prime * result + ((errno == null) ? 0 : errno.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Error other = (Error) obj;
    if (errmsg == null) {
      if (other.errmsg != null)
        return false;
    } else if (!errmsg.equals(other.errmsg))
      return false;
    if (errno == null) {
      if (other.errno != null)
        return false;
    } else if (!errno.equals(other.errno))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return errno + ": " + errmsg;
  }

}
