package zyxel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import zyxel.domain.Data;

@Controller
public class SiteController {
  @Autowired
  private FetchController fetchController;

  @RequestMapping("/")
  public String index() {
    return "index";
  }

  @RequestMapping(value = "/api/latestData")
  private ResponseEntity<Data> getLatestData() {
    return ResponseEntity.status(HttpStatus.OK).body(fetchController.getLatestData());
  }
}

